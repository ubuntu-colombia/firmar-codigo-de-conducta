#!/bin/bash

clear
main(){
  echo -e "Script para firma de código de conducta de Ubuntu, para iniciar es necesario que iniciemos sesión como sudo en tu equipo,\npara la instalación de dependencias y librerias necesarias"
  echo -e "\n"
  echo -e "Para esto es necesario ingres la contraseña de tu usuario de Ubuntu y presiones la tecla ENTER (al escribir no se verá nada)."
}

pause() {
  echo
  echo -n "> Presione la tecla 'c' para continuar o Ctrl+C para terminar el proceso (interrumpir la firma del código de conducta, deberás iniciar de nuevo) ... "
  while true; do
    read -n1 -r
    [[ $REPLY == 'c' ]] && break
  done
#  clear
  echo -e "\n Continuemos ...\n"
  echo "Paso $1 Terminado"
  echo
  clear
}

paso1(){
  echo -e "Paso 0"
  sudo apt update
  pause 0
}

paso2(){
echo "Paso 1) Crea una cuenta en https://launchpad.net - Si ya tienes una cuenta en launchpad inicia sesión en https://launchpad.net/+login"
pause 1
}

paso3(){
echo "Paso 2) Instalaré las librerías y paquetes necesarios, es posible que se te solicite nuevamente la clave de tu usuario para hacer sudo, luego aguarda un momento por favor..."

packetgnupg=$(sudo apt list --installed | grep gnupg)
packetwget=$(sudo apt list --installed | grep wget)
packetnano=$(sudo apt list --installed | grep nano)
packethaveged=$(sudo apt list --installed | grep haveged)
packetrngtools=$(sudo apt list --installed | grep rng-tools)
clear
echo $packethaveged
echo $packetnano
echo $packetwget
echo $packetgnupg
echo $packetrngtools

if [ -z "$packetgnupg" ] || [ -z "$packetwget" ] || [ -z "$packetnano" ] || [ -z "$packethaveged" ] || [ -z "$packetrngtools" ]
then
	paso22
else
	echo
fi

pause 2
}

paso22(){

sudo add-apt-repository universe
sudo apt update
sudo apt install -y gnupg wget nano haveged rng-tools
#clear
echo -e "\nLibrerías y paquetes instalados!"
pause 22
}

paso4(){
echo -e "\nPaso 3) Descargando código de conducta\n"
wget "https://launchpad.net/codeofconduct/2.0/+download" -O $HOME/UbuntuCodeofConduct-2.0.txt
echo -e "\nCódigo de conducta descargado"
pause 3
}

paso5(){
echo "Paso 4) Generando llaves GPG, selecciona la opción 1, es decir RSA and RSA (default), cuando te pida el tamaño, selecciona o ingresa 3072 presionando [Enter] y luego elije la opción sin caducidad escribiendo 0, te preguntará si la selección es correcta, presionas en S o Y, según corresponda, por último llena tus datos (Nombre, correo, etc) para finalizar presiona O, V o Y según corresponda en tu caso."
echo
echo "Luego de ingresar tus datos te pedirá una contraseña, para evitar complicaciones te recomendamos dejar este campo vacío, pero si deseas asignar una contraseña a tu llave GPG, puedes ingresarla, solo no la olvides ya que no hay manera de recuperarla"
echo
echo
echo "IMPORTANTE: Lee las instruccciones anteriores con detalle antes de continuar, ya que este paso es crítico durante el proceso."
echo
echo
read -p "Si leiste todo lo anterior, Presiona [Enter] para contiuar, si no dale una leida y luego presiona [Enter]"
echo
sudo rngd -r /dev/urandom
#gpg --gen-key
gpg --full-generate-key
gpg --list-keys
entropia=$(pidof rngd)
sudo kill -9 $entropia
echo -e "\nLlaves GPG generadas"
pause 4
}

paso6(){
echo "Paso 5) Estoy enviando tu llave GPG a los servidores de launchpad...\n"
llave8=$(gpg --fingerprint --keyid-format long | grep -P '\d{4}' | grep pub | grep -o -P '(?<=/)[A-Z0-9]{8}')
llave16=$(gpg --fingerprint --keyid-format long | grep -P '\d{4}' | grep pub | grep -o -P '(?<=/)[A-Z0-9]{16}')
sleep 6
gpg --send-keys --keyserver keyserver.ubuntu.com $llave8
gpg --send-keys --keyserver keyserver.ubuntu.com $llave16
clear
echo "Se han enviado llaves 8 y 16 a Launchpad.net"
pause 5
}

paso55(){

gpg --fingerprint $llave8 | grep =
gpg --fingerprint --keyid-format long $llave16 | grep =

echo
echo "Copia lo que está en la información que te aparece arribadespués del símbolo '=' (igual), es algo parecido a esto: AAAA BBBB CCCC DDDD EEEE FFFF GGGG HHHH IIII JJJJ, es posible que tengas varias llaves si has intentado varias veces el proceso, en ese caso deberás copiar la ÚLTIMA QUE APAREZCA EN LA LISTA"
echo
echo "Ahora ingresa a: (Cambia NombreDeUsuarioLP por tu nombre de usuario) https://launchpad.net/~NombreDeUsuarioLP/+editpgpkeys y pega el contenido copiado anteriormente, en el sitio web luego de copiar el dato, presiona en [Import Key]"
echo 
echo "Importante, si te aparece un aviso en ROJO que dice [Launchpad could not import your OpenPGP key] es posible que la llaves en Launchpad aún no se hayan sincronizado, debes esperar unos minutos e intentar pegar nuevamente tu llave GPG copiada anteriormente en el sitio web"
echo "Si evidencias que antes de copiar la llave ya aparece otra allí pendiente por validar seleccionala y dale donde dice [Cancel validarion for Selected Keys]"
echo
pause 55
}

paso7(){
echo "Paso 6) verifica tu correo electrónico, debe haber un mensaje nuevo con el asunto [Launchpad: Confirm your OpenPGP Key]"
echo
echo "En el correo que te ha llegado encontrarás un mensaje cifrado casi al final del correo"
echo "inicia con:  -----BEGIN PGP MESSAGE-----"
echo "finaliza con:  -----END PGP MESSAGE-----"
echo
echo "============================================="
echo "==========       IMPORTANTE     ============="
echo "============================================="
echo
echo "Es necesario que copies el contenido desde la sección de inicio hasta la sección de fin (es decir lo que está dentro de los -----) para pegarla en el siguiente paso"
echo "Una vez hayas pegado el texto debes presionar las teclas CONTROL + X y luego presiona [Y] [O] [S] según corresponda (ver en la parte inferior de la pantalla para ayuda, y luego presiona [Enter]"
pause x
rm -f correo.txt
nano correo.txt
clear
pause 6

echo "En este paso vamos a decodificar el correo cifrado enviado por launchpad, si a tu llave GPG le asignaste una contraseña deberás ingresarla en este paso para poder continuar"
echo
gpg -d correo.txt | grep https
echo
}

paso8(){
echo "Paso 7) Ingresa a la URL que aparece anteriormente para verificar tu correo electrónico, si te aparece algún mensaje de error intentalo varias veces hasta que aparezca un mensaje en azul, si no aparece la URL has realizado los pasos de manera incorrecta y te recomendamos iniciar de nuevo"
echo
echo "No continúes hasta que no valides el correo con el link anterior"
pause 7
}

paso9(){
echo "Paso 8) Firmar el código de conducta"
echo
echo "Estos son los últimos pasos, de los cuales sugerimos tener especial cuidado y leer las instrucciones con detalle"

last_line=$(gpg --list-public-keys --keyid-format short | grep pub | tail -n 1)
# Extraer el ID de la clave GPG de la última línea
key_id=$(echo $last_line | awk '{print $2}' | cut -d '/' -f 2)
#echo "$key_id"

rm -f $HOME/UbuntuCodeofConduct-2.0.txt.asc
#gpg --clearsign $HOME/UbuntuCodeofConduct-2.0.txt
gpg --clearsign --default-key $key_id UbuntuCodeofConduct-2.0.txt

echo "En el siguiente paso se mostrará tu código de conducta listo para firmar, debes copiar el contenido que aparece desde el inicio hasta el final"
echo
echo "TIP: Debes hacer Scroll con tu puntero para poder leer el contenido."
echo "El código inicia en una sección como esta: '-----BEGIN PGP SIGNED MESSAGE-----'"
echo "Luego verás un texto largo que es el código de conducta"
echo "El código finaliza en una sección como esta: '-----END PGP SIGNATURE-----' pasando por un texto cifrado"
echo "Deberás copiar todo el texto contenido entre esas dos secciones mencionadas (incluyendo la línea de inicio y fin), lo necesitaremos en el paso siguiente"
pause
clear
cat $HOME/UbuntuCodeofConduct-2.0.txt.asc
echo "Deberás copiar TODO lo que aparece arriba hasta el fin de la firma GPG del Código de conducta con la llave, para usarlo en el siguiente paso"
pause 8
}

paso10(){

echo "Lo que copiaste en el paso anterior deberás pegarlo en el siguiente paso..."
pause 9
}

paso11(){
echo "Ingresa a esta URL https://launchpad.net/codeofconduct/2.0/+sign , pega el texto COMPLETO del paso anterior y presiona el botón [Continue]"
pause 10
}

main
paso1
paso2
paso3
paso4
paso5
paso6
paso55
paso7
paso8
paso9
paso10
paso11

echo "Felicidades, has completado el proceso, para verificar que todo esté en orden, en tu perfil de Launchpad deberás ver una sección con esta información: Signed Ubuntu Code of Conduct:Yes"
echo "¡Gracias por querer ser parte de la familia Ubuntu! =) "
echo
echo "No olvides unirte a nuestro Team para ser miembro oficial de la comunidad: https://launchpad.net/~ubuntu-co "
